# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Edin Veskovic <edin.lockedin@gmail.com>, 2014
# Ivica  Kolić <ikoli@yahoo.com>, 2010
# Ivica  Kolić <ikoli@yahoo.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-22 12:32+0200\n"
"PO-Revision-Date: 2019-09-27 05:07+0000\n"
"Last-Translator: Ivica  Kolić <ikoli@yahoo.com>\n"
"Language-Team: Croatian (http://www.transifex.com/xfce/xfce-panel-plugins/language/hr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hr\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#. vim: set ai et tabstop=4:
#: ../panel-plugin/places.desktop.in.h:1 ../panel-plugin/cfg.c:100
#: ../panel-plugin/cfg.c:195 ../panel-plugin/cfg.c:396
msgid "Places"
msgstr "Mjesta"

#: ../panel-plugin/places.desktop.in.h:2
msgid "Access folders, documents, and removable media"
msgstr "Pristup mapama, dokumentima i uklonjivim medijima"

#. Trash
#: ../panel-plugin/model_system.c:152
msgid "Trash"
msgstr "Smeće"

#: ../panel-plugin/model_system.c:186
msgid "Desktop"
msgstr "Radna površina"

#. File System (/)
#: ../panel-plugin/model_system.c:202
msgid "File System"
msgstr "Datotečni sustav"

#. TRANSLATORS: this will result in "<path> on <hostname>"
#: ../panel-plugin/model_user.c:256
#, c-format
msgid "%s on %s"
msgstr "%s na %s"

#: ../panel-plugin/model_volumes.c:71
#, c-format
msgid "Failed to eject \"%s\""
msgstr "Neuspjeh u izbacivanju \"%s\""

#: ../panel-plugin/model_volumes.c:120
#, c-format
msgid "Failed to unmount \"%s\""
msgstr "Neuspjeh u  demontiranju \"%s\""

#: ../panel-plugin/model_volumes.c:170 ../panel-plugin/model_volumes.c:193
#, c-format
msgid "Failed to mount \"%s\""
msgstr "Neuspjeh u montiranju \"%s\""

#: ../panel-plugin/model_volumes.c:478
msgid "Mount and Open"
msgstr "Montiraj i otvori"

#: ../panel-plugin/model_volumes.c:491
msgid "Mount"
msgstr "Montiraj"

#: ../panel-plugin/model_volumes.c:511
msgid "Eject"
msgstr "Izbaci"

#: ../panel-plugin/model_volumes.c:521
msgid "Unmount"
msgstr "Demontiraj"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:128
msgid "Unmounting device"
msgstr "Odmontiranje uređaja"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:131
#, c-format
msgid ""
"The device \"%s\" is being unmounted by the system. Please do not remove the"
" media or disconnect the drive"
msgstr "Uređaj \"%s\" je odmontiran od strane sustava.Molim da ne uklanjajte medij ili isljučujete pogon"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:138
#: ../panel-plugin/model_volumes_notify.c:262
msgid "Writing data to device"
msgstr "Zapisujem podatke na uređaj"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:141
#: ../panel-plugin/model_volumes_notify.c:265
#, c-format
msgid ""
"There is data that needs to be written to the device \"%s\" before it can be"
" removed. Please do not remove the media or disconnect the drive"
msgstr "Postoje podaci koje treba zapisati na uređaj \"%s\" prije nego se uređaj ukloni. Molim nemojte uklanjati medij ili isključivati pogon."

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:253
msgid "Ejecting device"
msgstr "Izbacujem uređaj"

#. TRANSLATORS: Please use the same translation here as in Thunar
#: ../panel-plugin/model_volumes_notify.c:256
#, c-format
msgid "The device \"%s\" is being ejected. This may take some time"
msgstr "Uređaj \"%s\" se izbacuje. Ovo može potrajati"

#: ../panel-plugin/view.c:656
msgid "Search for Files"
msgstr "Traži datoteke"

#: ../panel-plugin/view.c:697
msgid "_Clear"
msgstr "_Očisti"

#. RECENT DOCUMENTS
#: ../panel-plugin/view.c:713 ../panel-plugin/cfg.c:555
msgid "Recent Documents"
msgstr "Nedavni dokumenti"

#: ../panel-plugin/cfg.c:399
msgid "Close"
msgstr "Zatvori"

#. BUTTON
#: ../panel-plugin/cfg.c:473
msgid "Button"
msgstr "Dugme"

#. BUTTON: Show Icon/Label
#: ../panel-plugin/cfg.c:478
msgid "_Show"
msgstr "_Pokaži"

#: ../panel-plugin/cfg.c:483
msgid "Icon Only"
msgstr "Samo ikona"

#: ../panel-plugin/cfg.c:484
msgid "Label Only"
msgstr "Samo natpis"

#: ../panel-plugin/cfg.c:485
msgid "Icon and Label"
msgstr "Ikona i natpis"

#. BUTTON: Label text entry
#: ../panel-plugin/cfg.c:494
msgid "_Label"
msgstr "_Natpis"

#. MENU
#: ../panel-plugin/cfg.c:506
msgid "Menu"
msgstr "Izbornik"

#. MENU: Show Icons
#: ../panel-plugin/cfg.c:511
msgid "Show _icons in menu"
msgstr "Pokaži _ikone u izborniku"

#. MENU: Show Removable Media
#: ../panel-plugin/cfg.c:519
msgid "Show _removable media"
msgstr "Pokaži _uklonjive medije"

#. MENU: - Mount and Open (indented)
#: ../panel-plugin/cfg.c:527
msgid "Mount and _Open on click"
msgstr "Montiraj i _otvori klikom"

#. MENU: Show GTK Bookmarks
#: ../panel-plugin/cfg.c:539
msgid "Show GTK _bookmarks"
msgstr "Pokaži GTK _zabilješke"

#. MENU: Show Recent Documents
#: ../panel-plugin/cfg.c:547
msgid "Show recent _documents"
msgstr "Pokaži nedavne _dokumente"

#. RECENT DOCUMENTS: Show clear option
#: ../panel-plugin/cfg.c:564
msgid "Show cl_ear option"
msgstr "Pokaži op_ciju čisti"

#. RECENT DOCUMENTS: Number to display
#: ../panel-plugin/cfg.c:576
msgid "_Number to display"
msgstr "_Broj za prikazati"

#. SEARCH
#: ../panel-plugin/cfg.c:600
msgid "Search"
msgstr "Traži"

#. Search: command
#: ../panel-plugin/cfg.c:605
msgid "Co_mmand"
msgstr "Na_redba"

#: ../panel-plugin/support.c:155
msgid "Open"
msgstr "Otvori"

#: ../panel-plugin/support.c:170
msgid "Open Terminal Here"
msgstr "Otvori terminal ovdje"

#: ../panel-plugin/xfce4-popup-places.sh:28
msgid "Usage:"
msgstr "Upotreba:"

#: ../panel-plugin/xfce4-popup-places.sh:29
msgid "OPTION"
msgstr "OPCIJA"

#: ../panel-plugin/xfce4-popup-places.sh:31
msgid "Options:"
msgstr "Opcije:"

#: ../panel-plugin/xfce4-popup-places.sh:32
msgid "Popup menu at current mouse position"
msgstr "Skočni izbornik na trenutnoj poziciji miša"

#: ../panel-plugin/xfce4-popup-places.sh:33
msgid "Show help options"
msgstr "Pokaži opcije pomoći"

#: ../panel-plugin/xfce4-popup-places.sh:34
msgid "Print version information and exit"
msgstr "Ispiši informacije o verziji i izađi"
